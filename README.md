# Enhancing HTML Structure Comprehension: Real-Time 3D/XR Visualization of the DOM - Replication package

## Table of Contents

A high level of this reproduction package doc.

- [Enhancing HTML Structure Comprehension: Real-Time 3D/XR Visualization of the DOM - Replication package](#enhancing-html-structure-comprehension-real-time-3dxr-visualization-of-the-dom---replication-package)
  - [Table of Contents](#table-of-contents)
  - [Scene](#scene)
    - [Deploy them locally](#deploy-them-locally)
  - [Visual Studio Plugin](#visual-studio-plugin)
  - [Setting up AR real-time visualization](#setting-up-ar-real-time-visualization)
  - [Demo video](#demonstration-video)
  - [babia-html API doc](#babia-html-api-doc)

## Scene

There are two scenes prepared for this plugin you can visit clicking on the following links:

- [`simple.html`](https://dlumbrer.gitlab.io/2024-vissoft-nier-reppkg/simple.html): simple HTML structure.
- [`complex.html`](https://dlumbrer.gitlab.io/2024-vissoft-nier-reppkg/complex.html): complex HTML structure.

The code can be visited inside the `scene` folder.

Also, we provide the RAW HTML codes without the scene, for rendering them in a browser:

- [`raw_html/simple.html`](https://dlumbrer.gitlab.io/2024-vissoft-nier-reppkg/raw/simple.html): simple HTML structure.
- [`raw_html/complex.html`](https://dlumbrer.gitlab.io/2024-vissoft-nier-reppkg/raw/complex.html): complex HTML structure.


### Deploy them locally

The easiest way to do it is to go to the `scene` folder and deploy the html files within a simple HTTP server (i.e. node http-server or python simple http server).

(Optional) Examples of http servers deploy:
```shell
# Using node [1]
$> npm install -g http-server
$> http-server

# Using python 2 [2]
$> python -m SimpleHTTPServer

# Using python 3 [3]
$> python -m http.server
```

## Visual Studio Plugin

The plugin can be installed via the official Marketplace:

- [Plugin in the Marketplace](https://marketplace.visualstudio.com/items?itemName=dlumbrer.html-in-xr)

Or installed via the `vsix/html-in-xr-1.0.5.vsix` file included in this replication package.

Source Code is available at:

- [Source Code](https://gitlab.io/dlumbrer/html-in-xr)


## Setting up AR real-time visualization

For setting up the visualization in AR in real-time you need to do the following:

1. Install plugin in a VSCode.
2. Execute the plugin
3. Copy the `raw_html/scene_realtime.html` into the same folder where the Visual Studio and the plugin is executed.
4. Launch a http-server **ON SSL MODE** that serves the `raw_html/scene_realtime_ar.html` copied.
5. Visit the scene in your AR glasses.
6. When you update the HTML of your IDE the visualization is updated after 0-5 seconds. 

## Demonstration video

Here is the link of the demonstration video of the tool presented:

- [YouTube video URL](https://youtu.be/RFX2gblBNss)

## babia-html API doc

Visit [BabiaXR](https://babiaxr.gitlab.io) for more information about the visualizations, and the [`babia-html` component API](https://babiaxr.gitlab.io/apis/others/#babia-html-component) for customizing the visualization.